import os
import requests
import json


def load_points(file_path):
    points = []
    with open(file_path) as file:
        for line in file.readlines()[1:]:
            words = line.strip().split(",")
            points.append({"x1": float(words[1]), "y1": float(words[2]), "x2": float(words[3]), "y2": float(words[4])})
    return points


def load_requests(points, count):
    requests_list = []
    for i in range(0, count):
        data = {
                    "alternative": 1,
                    "locale": "ru",
                    "type": "jam",
                    "points": [
                        {
                            "start": True,
                            "type": "pedo",
                            "x": points[i]['x1'],
                            "y": points[i]['y1']
                        },
                        {
                            "start": False,
                            "type": "pedo",
                            "x": points[i]['x2'],
                            "y": points[i]['y2']
                        }
                    ]
                }
        requests_list.append(data)
    return requests_list


def run_load(url1, requests_list):
    distance_list = []
    for i, request in enumerate(requests_list):
        request_body = json.dumps(request)
        resp = requests.post(url1, data=request_body)
        resp.raise_for_status()

        json_resp = resp.json()
        #print(json_resp)
        distance = json_resp['result'][0]['total_distance']
        distance_list.append(distance)

    data = ['distance']
    for i in range(len(distance_list)):
        text = f'{distance_list[i]}'

        data.append(text)

    result_file = open('distance_results.csv', 'w')  # файлик с результатами, с новым прогоном сохраняются новые данные
    result_file.write('\n'.join(data) + '\n')
    result_file.close()


points = load_points('../carrouting-points.csv')
count = len(points)
requests_list = load_requests(points, 100)

url1 = 'http://moses3.test.n3.navi/carrouting/4.0.0/global'
run_load(url1=url1, requests_list=requests_list)
