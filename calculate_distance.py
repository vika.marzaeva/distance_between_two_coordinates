import statistics
from geopy.distance import geodesic


def load_points(file_path):
    points = []
    with open(file_path) as file:
        for line in file.readlines()[1:]:
            words = line.strip().split(",")
            points.append({"x1": float(words[1]), "y1": float(words[2]), "x2": float(words[3]), "y2": float(words[4])})
    return points


def distance_calculation(points, count):
    data = ['x1, y1, x2, y2, distance']
    for i in range(0, count):
        x1 = points[i]['x1']
        y1 = points[i]['y1']
        x2 = points[i]['x2']
        y2 = points[i]['y2']
        distance = (geodesic((x1, y1), (x2, y2)). m)
        #print(distance)
        text = f'{x1},' \
               f'{y1}, ' \
               f'{x2}, ' \
               f'{y2}, ' \
               f'{distance}'

        data.append(text)

    result_file = open('carrouting-distance.csv', 'w+')
    result_file.write('\n'.join(data) + '\n')
    result_file.close()


def distance_analyse(result_file):
    distance_list = []
    with open(result_file) as file:
        for line in file.readlines()[1:]:
            words = line.strip().split(",")
            distance_list.append(float(words[4]))
    print(max(distance_list))
    print(statistics.mean(distance_list))
    print(min(distance_list))


points = load_points('carrouting-points.csv')
count = len(points)
distance_calculation(points, count)
result_file = 'carrouting-distance.csv'
distance_analyse(result_file)

